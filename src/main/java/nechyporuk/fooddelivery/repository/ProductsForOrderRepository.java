package nechyporuk.fooddelivery.repository;

import nechyporuk.fooddelivery.entity.ProductsForOrder;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsForOrderRepository extends JpaRepository<ProductsForOrder, Long> {
}
