package nechyporuk.fooddelivery.repository;

import nechyporuk.fooddelivery.entity.Category;
import nechyporuk.fooddelivery.entity.Products;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductsRepository extends JpaRepository<Products, Long> {

    Page<Products> findAllByCategory(Category category, Pageable pageable);
}
