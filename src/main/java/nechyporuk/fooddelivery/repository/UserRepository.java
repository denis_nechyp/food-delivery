package nechyporuk.fooddelivery.repository;

import nechyporuk.fooddelivery.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLoginEquals(String login);
}