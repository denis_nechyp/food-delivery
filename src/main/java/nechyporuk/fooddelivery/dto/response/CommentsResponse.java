package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.Comments;

import java.time.LocalDateTime;

@Getter @Setter
public class CommentsResponse {
    private Long id;

    private String content;

    private LocalDateTime dateTime;

    private String clientsFirstName;

    private String clientsLastName;


    public CommentsResponse(Comments comments) {
        id = comments.getId();
        content = comments.getContent();
        dateTime = comments.getDateTime();
        clientsFirstName = comments.getClient().getFirstName();
        clientsLastName = comments.getClient().getLastName();
    }
}
