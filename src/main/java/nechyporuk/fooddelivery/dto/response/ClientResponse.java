package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.Client;

@Getter
@Setter
@NoArgsConstructor
public class ClientResponse {
    private Long id;

    private String firstName;

    private String lastName;

    private String telephoneNumber;

    private String fullAddress;

    private Long addressId;


    public ClientResponse(Client client) {
        id = client.getId();
        firstName = client.getFirstName();
        lastName = client.getLastName();
        telephoneNumber = client.getTelephoneNumber();
        fullAddress =  client.getAddress().getCity() + ", " + client.getAddress().getStreet() + ", "
                + client.getAddress().getApartmentNumber();
        addressId = client.getAddress().getId();
    }
}
