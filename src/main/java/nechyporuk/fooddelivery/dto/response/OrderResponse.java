package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.Order;
import nechyporuk.fooddelivery.entity.ProductsForOrder;

import java.time.LocalDateTime;
import java.util.List;

@Getter @Setter
public class OrderResponse {

    private Long id;

    private String note;

    private LocalDateTime dateTime;

    private String clientName;

    private Integer payment;


    public OrderResponse(Order order) {
        id = order.getId();
        note = order.getNote();
        dateTime = order.getDateTime();
        clientName = order.getClient().getFirstName();
        payment = order.getProductsForOrders().size();
    }
}
