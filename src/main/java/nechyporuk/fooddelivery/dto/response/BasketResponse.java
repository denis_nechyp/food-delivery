package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.Basket;

import java.util.List;

@Getter @Setter
public class BasketResponse {

    private Long id;

    private String fullName;

    public BasketResponse(Basket basket) {
        id = basket.getId();
        fullName = basket.getClient().getFirstName() + basket.getClient().getLastName();
    }
}
