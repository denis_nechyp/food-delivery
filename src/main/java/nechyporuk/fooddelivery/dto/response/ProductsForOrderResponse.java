package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.ProductsForOrder;

@Getter @Setter
public class ProductsForOrderResponse {
    private Long id;

    private Integer count;

    private String productsName;


    public ProductsForOrderResponse(ProductsForOrder productsForOrder) {
        id = productsForOrder.getId();
        count = productsForOrder.getCount();
        productsName = productsForOrder.getProducts().getName();
    }
}
