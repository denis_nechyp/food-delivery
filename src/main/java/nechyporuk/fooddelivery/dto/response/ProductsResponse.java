package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.Products;

@Getter @Setter
public class ProductsResponse {
    private Long id;

    private Long categoryId;

    private String name;

    private String picture;

    private Integer price;

    private Integer size;

    private Integer weight;

    private Integer volume;

    private String ingredients;

    private String categoryName;

    public ProductsResponse(Products products) {
        id = products.getId();
        categoryId = products.getCategory().getId();
        name = products.getName();
        picture = products.getPicture();
        price = products.getPrice();
        size = products.getSize();
        weight = products.getWeight();
        volume = products.getVolume();
        ingredients = products.getIngredients();
        categoryName = products.getCategory().getName();
    }
}
