package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.Category;

@Getter
@Setter
@NoArgsConstructor

public class CategoryResponse {
    private Long id;

    private String name;

    public CategoryResponse(Category category) {
        id = category.getId();
        name = category.getName();
    }
}
