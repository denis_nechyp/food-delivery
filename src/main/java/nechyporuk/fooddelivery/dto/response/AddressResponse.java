package nechyporuk.fooddelivery.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import nechyporuk.fooddelivery.entity.Address;

@Getter
@Setter
@NoArgsConstructor

public class AddressResponse {
    private Long id;

    private String city;

    private String street;

    private String apartmentNumber;


    public AddressResponse(Address address) {
        id = address.getId();
        city = address.getCity();
        street = address.getStreet();
        apartmentNumber = address.getApartmentNumber();
    }
}
