package nechyporuk.fooddelivery.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter @Setter
@NoArgsConstructor
public class OrderRequest {

    private String note;

    private Long clientId;

    private List<ProductsForOrderRequest> productsForOrdersId;
}
