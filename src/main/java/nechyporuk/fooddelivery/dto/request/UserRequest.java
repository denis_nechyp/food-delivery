package nechyporuk.fooddelivery.dto.request;


import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRequest {

    private String login;

    private String password;

    private Long clientId;
}
