package nechyporuk.fooddelivery.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ProductsRequest {
    private String name;

    private String picture;

    private Integer price;

    private Integer size;

    private Integer weight;

    private Integer volume;

    private String ingredients;

    private Long categoryId;
}
