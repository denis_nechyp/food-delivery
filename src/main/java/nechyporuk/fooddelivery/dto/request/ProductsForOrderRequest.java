package nechyporuk.fooddelivery.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter @Setter
@NoArgsConstructor
public class ProductsForOrderRequest {
    private Integer count;

    private Long productsId;
}
