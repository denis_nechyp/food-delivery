package nechyporuk.fooddelivery.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class FileRequest {
    private String data;
    private String fileName;
}
