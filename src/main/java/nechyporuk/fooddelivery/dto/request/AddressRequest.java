package nechyporuk.fooddelivery.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
@NoArgsConstructor

public class AddressRequest {
   @NotBlank
   private String city;

   @NotBlank
   private String street;

   @NotBlank
   private String apartmentNumber;
}
