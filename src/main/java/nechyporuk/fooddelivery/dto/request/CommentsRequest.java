package nechyporuk.fooddelivery.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter
@NoArgsConstructor
public class CommentsRequest {
    private String content;

    private LocalDateTime dateTime;

    private Long clientId;

    private Long productsId;
}
