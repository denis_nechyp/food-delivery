package nechyporuk.fooddelivery.dto.request;

import lombok.Getter;
import lombok.Setter;

@Getter @Setter
public class ClientFilterRequest {

    private String value;

    private PaginationRequest paginationRequest;
}
