package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.UserRequest;
import nechyporuk.fooddelivery.entity.Role;
import nechyporuk.fooddelivery.entity.User;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.UserRepository;
import nechyporuk.fooddelivery.security.tokenUtils.TokenTool;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserService {


    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private TokenTool tokenTool;

    @Autowired
    private ClientService clientService;


    public String save(UserRequest request) throws Exception {
        if (userRepository.findByLoginEquals(request.getLogin()).isPresent()) {
            throw new Exception("Credentials are busy. Please, try one more time " +
                    "with other logib");
        }
        User user = new User();
        user.setLogin(request.getLogin());
        user.setPassword(passwordEncoder.encode(request.getPassword()));
        user.setRole(Role.USER);
        user.setClient(clientService.findOne(request.getClientId()));

        user = userRepository.saveAndFlush(user);

        return tokenTool.createToken(user.getLogin(), user.getRole().name());
    }


    public String findOneByRequest(UserRequest userRequest) throws WrongInputException {
        User user = userRepository.findByLoginEquals(userRequest.getLogin()).orElseThrow(() -> new WrongInputException("User with login " + userRequest.getLogin() + " not exists"));

        if (passwordEncoder.matches(userRequest.getPassword(), user.getPassword())) {
            return tokenTool.createToken(user.getLogin(), user.getRole().name());
        }

        throw new IllegalArgumentException("Wrong login or password");
    }
}
