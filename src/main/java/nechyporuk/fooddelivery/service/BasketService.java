package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.BasketRequest;
import nechyporuk.fooddelivery.dto.response.BasketResponse;
import nechyporuk.fooddelivery.entity.Basket;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.BasketRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class BasketService {

    @Autowired
    private BasketRepository basketRepository;

    public List<BasketResponse> findAll() {
        return basketRepository.findAll().stream().map(BasketResponse::new).collect(Collectors.toList());
    }

    public Basket findOne(Long id) throws WrongInputException {
        return basketRepository.findById(id).orElseThrow(() -> new WrongInputException("There is no such basket, with id " + id));
    }

    private Basket basketRequestToBasket(Basket basket, BasketRequest request) {
        if (basket == null) {
            basket = new Basket();
        }
        return basketRepository.save(basket);
    }

    public BasketResponse save(BasketRequest request) {
        return new BasketResponse(basketRequestToBasket(null, request));
    }

    public BasketResponse update(Long id, BasketRequest request) throws WrongInputException {
        return new BasketResponse(basketRequestToBasket(findOne(id), request));
    }

    public void delete(Long id) throws WrongInputException {
        basketRepository.delete(findOne(id));
    }
}
