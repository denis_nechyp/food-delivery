package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.PaginationRequest;
import nechyporuk.fooddelivery.dto.request.ProductsRequest;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.dto.response.ProductsResponse;
import nechyporuk.fooddelivery.entity.Category;
import nechyporuk.fooddelivery.entity.Products;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.ProductsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductsService {

    @Autowired
    private ProductsRepository productsRepository;

    @Autowired
    private CategoryService categoryService;

    public List<ProductsResponse> findAll() {
        return productsRepository.findAll().stream()
                .map(ProductsResponse::new)
                .collect(Collectors.toList());
    }

    private Products productsRequestToProducts(ProductsRequest request, Products products) throws WrongInputException {
        if (products == null) {
            products = new Products();
        }
        products.setName(request.getName());
        products.setPicture(request.getPicture());  //Here must be a picture
        products.setPrice(request.getPrice());
        products.setSize(request.getSize());
        products.setWeight(request.getWeight());
        products.setVolume(request.getVolume());
        products.setIngredients(request.getIngredients());
        products.setCategory(categoryService.findOne(request.getCategoryId()));
        return productsRepository.save(products);
    }

    public ProductsResponse findOneById(Long id ) throws WrongInputException {
        return new ProductsResponse(findOne(id));
    }

    public Products findOne(Long id) throws WrongInputException {
        return productsRepository.findById(id).orElseThrow(() -> new WrongInputException("There is no such products, with id " + id));
    }

    public ProductsResponse save(ProductsRequest request) throws WrongInputException {
        return new ProductsResponse(productsRequestToProducts(request, null));
    }

    public ProductsResponse update(ProductsRequest request, Long id) throws WrongInputException {
        return new ProductsResponse(productsRequestToProducts(request, findOne(id)));
    }

    public void delete(Long id) throws WrongInputException {
        productsRepository.delete(findOne(id));
    }

    public DataResponse<ProductsResponse> findAll(PaginationRequest pagination) {
        Page<Products> all = productsRepository.findAll(pagination.mapToPageRequest());
        return new DataResponse<>(all.get().map(ProductsResponse::new).collect(Collectors.toList()), all.getTotalPages(), all.getTotalElements());
    }

        public DataResponse<ProductsResponse> findAllByCategoryId(Long categoryId, PaginationRequest paginationRequest) throws WrongInputException {
            Category category = categoryService.findOne(categoryId);
            Page<Products> allByCategory = productsRepository.findAllByCategory(category, paginationRequest.mapToPageRequest());
            return new DataResponse<>(allByCategory.get().map(ProductsResponse::new).collect(Collectors.toList()),
                    allByCategory.getTotalPages(), allByCategory.getTotalElements());
        }
}
