package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.ProductsForOrderRequest;
import nechyporuk.fooddelivery.dto.response.ProductsForOrderResponse;
import nechyporuk.fooddelivery.entity.ProductsForOrder;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.ProductsForOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductsForOrderService {

    @Autowired
    private ProductsForOrderRepository productsForOrderRepository;

    @Autowired
    private ProductsService productsService;

    public List<ProductsForOrderResponse> findAll() {
        return productsForOrderRepository.findAll().stream()
                .map(ProductsForOrderResponse::new)
                .collect(Collectors.toList());
    }

    private ProductsForOrder productsForOrderRequestToProductsForOrder(ProductsForOrderRequest request, ProductsForOrder productsForOrder) throws WrongInputException {
        if (productsForOrder == null) {
            productsForOrder = new ProductsForOrder();
        }
        productsForOrder.setCount(request.getCount());
        productsForOrder.setProducts(productsService.findOne(request.getProductsId()));
        return productsForOrderRepository.save(productsForOrder);
    }

    public ProductsForOrder findOne(Long id) throws WrongInputException {
        return productsForOrderRepository.findById(id).orElseThrow(() -> new WrongInputException("There is no such products for order, with id " + id));
    }

    public ProductsForOrderResponse save (ProductsForOrderRequest request) throws WrongInputException {
        return new ProductsForOrderResponse(productsForOrderRequestToProductsForOrder(request, null));
    }

    public ProductsForOrderResponse update (ProductsForOrderRequest request, Long id) throws WrongInputException {
        return new ProductsForOrderResponse(productsForOrderRequestToProductsForOrder(request, findOne(id)));
    }

    public void delete(Long id) throws WrongInputException {
        productsForOrderRepository.delete(findOne(id));
    }
}
