package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.AddressRequest;
import nechyporuk.fooddelivery.dto.request.PaginationRequest;
import nechyporuk.fooddelivery.dto.response.AddressResponse;
import nechyporuk.fooddelivery.dto.response.CategoryResponse;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.entity.Address;
import nechyporuk.fooddelivery.entity.Category;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class AddressService {

    @Autowired
    private AddressRepository addressRepository;

    public List<AddressResponse> findAll() {
        return addressRepository.findAll().stream()
                .map(AddressResponse::new)
                .collect(Collectors.toList());
    }

    public AddressResponse save(AddressRequest request) {
        return new AddressResponse(addressRequestToAddress(request, null));
    }

    public AddressResponse update(AddressRequest request, Long id) throws WrongInputException {
        return new AddressResponse(addressRequestToAddress(request, findOne(id)));
    }

    private Address addressRequestToAddress(AddressRequest request, Address address) {
        if (address == null) {
            address = new Address();
        }
        address.setCity(request.getCity());
        address.setStreet(request.getStreet());
        address.setApartmentNumber(request.getApartmentNumber());
        return addressRepository.save(address);
    }

    public AddressResponse findOneById(Long id) throws WrongInputException {
        return new AddressResponse(findOne(id));
    }

    public void delete(Long id) throws WrongInputException {
        addressRepository.delete(findOne(id));
    }

    public Address findOne(Long id) throws WrongInputException {
        return addressRepository.findById(id)
                .orElseThrow(() -> new WrongInputException("There is no such address, with id " + id));
    }

}
