package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.CommentsRequest;
import nechyporuk.fooddelivery.dto.response.CommentsResponse;
import nechyporuk.fooddelivery.entity.Comments;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.CommentsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CommentsService {

    @Autowired
    private CommentsRepository commentsRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProductsService productsService;

    public List<CommentsResponse> findAll() {
        return commentsRepository.findAll().stream()
                .map(CommentsResponse::new)
                .collect(Collectors.toList());
    }

    private Comments commentsRequestToComments(CommentsRequest request, Comments comments) throws WrongInputException {
        if (comments == null) {
            comments = new Comments();
        }
        comments.setContent(request.getContent());
        comments.setClient(clientService.findOne(request.getClientId()));
        comments.setDateTime(LocalDateTime.now());
//        comments.setProducts(productService.findOne(request.getProductsId()));
        return commentsRepository.save(comments);
    }

    public Comments findOne(Long id) throws WrongInputException {
        return commentsRepository.findById(id).orElseThrow(() -> new WrongInputException("There is no such comment, with id " + id));
    }

    public CommentsResponse save(CommentsRequest request) throws WrongInputException {
        return new CommentsResponse(commentsRequestToComments(request, null));
    }

    public CommentsResponse update(CommentsRequest request, Long id) throws WrongInputException {
        return new CommentsResponse(commentsRequestToComments(request, findOne(id)));
    }

    public void delete(Long id) throws WrongInputException {
        commentsRepository.delete(findOne(id));
    }
}