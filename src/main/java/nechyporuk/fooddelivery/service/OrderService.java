package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.OrderRequest;
import nechyporuk.fooddelivery.dto.request.PaginationRequest;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.dto.response.OrderResponse;
import nechyporuk.fooddelivery.entity.Order;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProductsForOrderService productsForOrderService;

    public List<OrderResponse> findAll() {
        return orderRepository.findAll().stream().map(OrderResponse::new).collect(Collectors.toList());
    }

    public Order findOne(Long id) throws WrongInputException {
        return orderRepository.findById(id).orElseThrow(() -> new WrongInputException("There is no such order, with id " + id));
    }

    private Order orderResponseToOrder(Order order, OrderRequest request) throws WrongInputException {
        if (order == null) {
            order = new Order();
        }
        order.setNote(request.getNote());
        order.setDateTime(LocalDateTime.now());
        order.setClient(clientService.findOne(request.getClientId()));
        return orderRepository.save(order);
    }

    public OrderResponse save(OrderRequest request) throws WrongInputException {
        return new OrderResponse(orderResponseToOrder(null, request));
    }

    public OrderResponse update(Long id, OrderRequest request) throws WrongInputException {
        return new OrderResponse(orderResponseToOrder(findOne(id), request));
    }

    public void delete(Long id) throws WrongInputException {
        orderRepository.delete(findOne(id));
    }

    public DataResponse<OrderResponse> findAll(PaginationRequest pagination) {
        Page<Order> all = orderRepository.findAll(pagination.mapToPageRequest());
        return new DataResponse<>(all.get().map(OrderResponse::new).collect(Collectors.toList()), all.getTotalPages(), all.getTotalElements());
    }
}
