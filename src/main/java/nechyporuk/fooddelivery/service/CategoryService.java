package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.CategoryRequest;
import nechyporuk.fooddelivery.dto.request.PaginationRequest;
import nechyporuk.fooddelivery.dto.response.CategoryResponse;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.entity.Category;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public List<CategoryResponse> findAll() {
        return categoryRepository.findAll().stream()
                .map(CategoryResponse::new)
                .collect(Collectors.toList());
    }

    public CategoryResponse save(CategoryRequest request) {
        return new CategoryResponse(categoryRequestToCategory(request, null));
    }

    public CategoryResponse update(CategoryRequest request, Long id) throws WrongInputException {
        return new CategoryResponse(categoryRequestToCategory(request, findOne(id)));
    }

    private Category categoryRequestToCategory(CategoryRequest request, Category category) {
        if (category == null) {
            category = new Category();
        }
        category.setName(request.getName());
        return  categoryRepository.save(category);
    }

    public void delete(Long id) throws WrongInputException {
        categoryRepository.delete(findOne(id));
    }

    public CategoryResponse findOneById(Long id) throws WrongInputException {
        return new CategoryResponse(findOne(id));
    }

    public Category findOne(Long id) throws WrongInputException {
        return categoryRepository.findById(id).orElseThrow(() -> new WrongInputException("There is no such category, with id " + id));
    }

    public DataResponse<CategoryResponse> findAll(PaginationRequest paginationRequest) {
        Page<Category> all = categoryRepository.findAll(paginationRequest.mapToPageRequest());
        return new DataResponse<>(all.get().map(CategoryResponse::new).collect(Collectors.toList()),
                all.getTotalPages(), all.getTotalElements());
    }

}
