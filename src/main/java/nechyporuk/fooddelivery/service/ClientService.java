package nechyporuk.fooddelivery.service;

import nechyporuk.fooddelivery.dto.request.BasketRequest;
import nechyporuk.fooddelivery.dto.request.ClientFilterRequest;
import nechyporuk.fooddelivery.dto.request.ClientRequest;
import nechyporuk.fooddelivery.dto.response.BasketResponse;
import nechyporuk.fooddelivery.dto.response.ClientResponse;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.entity.Basket;
import nechyporuk.fooddelivery.entity.Client;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.repository.ClientRepository;
import nechyporuk.fooddelivery.specification.ClientSpecification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ClientService {

    @Autowired
    private ClientRepository clientRepository;

    @Autowired
    private AddressService addressService;

    @Autowired
    private BasketService basketService;

    public List<ClientResponse> findAll() {
        return clientRepository.findAll().stream()
                .map(ClientResponse::new)
                .collect(Collectors.toList());
    }

    private Client clientRequestToClient(Client client,ClientRequest request) throws WrongInputException {
        if (client == null) {
            client = new Client();
        }
        client.setFirstName(request.getFirstName());
        client.setLastName(request.getLastName());
        client.setTelephoneNumber(request.getTelephoneNumber());
        client.setAddress(addressService.findOne(request.getAddressId()));
        return clientRepository.save(client);
    }

    public ClientResponse save(ClientRequest request) throws WrongInputException {
        return new ClientResponse(clientRequestToClient(null, request));
    }

    public ClientResponse update (Long id, ClientRequest request) throws WrongInputException {
        return new ClientResponse(clientRequestToClient(findOne(id), request));
    }

    public void delete(Long id) throws WrongInputException {
        clientRepository.delete(findOne(id));
    }

    public Client findOne(Long id) throws WrongInputException {
        return clientRepository.findById(id).orElseThrow(() -> new WrongInputException("There is no such client, with id " + id));
    }

    public DataResponse<ClientResponse> findAllByFilter(ClientFilterRequest request) {
        Page<Client> allByFilter = clientRepository.findAll(new ClientSpecification(request), request.getPaginationRequest().mapToPageRequest());
        return new DataResponse<>(allByFilter.get().map(ClientResponse::new).collect(Collectors.toList()),
                allByFilter.getTotalPages(), allByFilter.getTotalElements());
    }

    public ClientResponse findOneById(Long id) throws WrongInputException {
        return new ClientResponse(findOne(id));
    }
}
