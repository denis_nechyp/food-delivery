package nechyporuk.fooddelivery.specification;

import nechyporuk.fooddelivery.dto.request.ClientFilterRequest;
import nechyporuk.fooddelivery.entity.Client;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;

public class ClientSpecification implements Specification<Client> {

    private ClientFilterRequest filter;

    public ClientSpecification(ClientFilterRequest filter) {
        this.filter = filter;
    }

    @Override
    public Predicate toPredicate(Root<Client> r, CriteriaQuery<?> cq, CriteriaBuilder cb) {

        if (filter.getValue() == null) {
            return null;
        }

        List<Predicate> predicates = new ArrayList<>();
        if (filter.getValue() != null && !filter.getValue().trim().isEmpty()) {
            predicates.add(cb.like(r.get("firstName"),"%" + filter.getValue() + "%"));
        }
        if (filter.getValue() != null && !filter.getValue().trim().isEmpty()) {
            predicates.add(cb.like(r.get("lastName"),"%" + filter.getValue() + "%"));
        }
        if (filter.getValue() != null && !filter.getValue().trim().isEmpty()) {
            predicates.add(cb.like(r.get("email"),"%" + filter.getValue() + "%"));
        }
        if (filter.getValue() != null && !filter.getValue().trim().isEmpty()) {
            predicates.add(cb.like(r.get("telephoneNumber"),"%" + filter.getValue() + "%"));
        }
//        if (filter.getLastName() != null && !filter.getLastName().trim().isEmpty()) {
//            predicates.add(cb.like(r.get("firstName"), "%" + filter.getFirstName() + "%"));
//        }
//        if (filter.getEmail() != null && !filter.getEmail().trim().isEmpty()) {
//            predicates.add(cb.like(r.get("email"), "%" + filter.getFirstName() + "%"));
//        }
//        if (filter.getTelephoneNumber() != null && filter.getTelephoneNumber().trim().isEmpty()) {
//            predicates.add(cb.like(r.get("telephoneNumber"), "%" + filter.getFirstName() + "%"));
//        }
        return cb.or(predicates.toArray(new Predicate[0]));

    }


}
