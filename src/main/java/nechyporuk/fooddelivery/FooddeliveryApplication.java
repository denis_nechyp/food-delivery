package nechyporuk.fooddelivery;

import nechyporuk.fooddelivery.entity.*;
import nechyporuk.fooddelivery.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class FooddeliveryApplication {


    @PostConstruct
    public void init() {

//        Characteristic characteristic = new Characteristic();
//        characteristic.setName("size");
//        characteristicRepository.save(characteristic);
//
//        Characteristic characteristic2 = new Characteristic();
//        characteristic2.setName("weight");
//        characteristicRepository.save(characteristic2);
//
//        Characteristic characteristic3 = new Characteristic();
//        characteristic3.setName("volume");
//        characteristicRepository.save(characteristic3);
//
//        Characteristic characteristic4 = new Characteristic();
//        characteristic4.setName("price");
//        characteristicRepository.save(characteristic4);
//
//        Characteristic characteristic5 = new Characteristic();
//        characteristic5.setName("ingredients");
//        characteristicRepository.save(characteristic5);
//
//
//        Category category = new Category();
//        category.setName("pizza");
//        categoryRepository.save(category);
//
//        Category category2 = new Category();
//        category2.setName("sushi");
//        categoryRepository.save(category2);
//
//        Category category3 = new Category();
//        category3.setName("drink");
//        categoryRepository.save(category3);
//
//
//        Products products = new Products();
//        products.setName("Paperony");
//        products.setCategory(category);
//        productsRepository.save(products);
//
//        Products products2 = new Products();
//        products2.setName("Fish Roll");
//        products2.setCategory(category2);
//        productsRepository.save(products2);
//
//        Products products3 = new Products();
//        products3.setName("Morshinska");
//        products3.setCategory(category3);
//        productsRepository.save(products3);
//
//        Products products4 = new Products();
//        products4.setName("Cheeze pizza");
//        products4.setCategory(category);
//        productsRepository.save(products4);


//        Address address = new Address();
//        address.setCity("Lviv");
//        address.setStreet("Lukasha 4");
//        address.setApartmentNumber("503");
//
//        Comments comments = new Comments();
//        comments.setComment("The food was really tasty");
//
    }


    public static void main(String[] args) {
        SpringApplication.run(FooddeliveryApplication.class, args);
    }

}

