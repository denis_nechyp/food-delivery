package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.AddressRequest;
import nechyporuk.fooddelivery.dto.response.AddressResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public AddressResponse save(@RequestBody @Valid AddressRequest addressRequest) {
        return addressService.save(addressRequest);
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public List<AddressResponse> findAll() {
        return addressService.findAll();
    }

    @GetMapping("/getOneById")
    public AddressResponse findOneById(@RequestParam Long id) throws WrongInputException {
        return addressService.findOneById(id);
    }

    @PutMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public AddressResponse update(@RequestBody @Valid AddressRequest addressRequest, @RequestParam Long id) throws WrongInputException {
        return addressService.update(addressRequest, id);
    }

    @DeleteMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void delete(@RequestParam Long id) throws WrongInputException {
        addressService.delete(id);
    }
}
