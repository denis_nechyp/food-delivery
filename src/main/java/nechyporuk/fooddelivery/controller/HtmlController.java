package nechyporuk.fooddelivery.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HtmlController {

    @RequestMapping("/adminCategory")
    public String categoryPage() {
        return "adminCategory.html";
    }

    @RequestMapping("/adminProducts")
    public String productPage() {
        return "adminProducts.html";
    }

    @RequestMapping("/adminClient")
    public String client() {
        return "adminClient.html";
    }

    @RequestMapping("/adminAddress")
    public String address() {
        return "adminAddress.html";
    }

    @RequestMapping("/registration")
    public String registration() { return "registration.html"; }

    @RequestMapping("/sing")
    public String sing() { return "sing.html"; }

    @RequestMapping("/main")
    public String mainPage() {
        return "mainPage.html";
    }

    @RequestMapping("/category-sushi")
    public String sushiPage() {
        return "category-sushi.html";
    }

    @RequestMapping("/category-soup")
    public String soupPage() {
        return "category-soup.html";
    }

    @RequestMapping("/category-salad")
    public String saladPage() {
        return "category-salad.html";
    }

    @RequestMapping("/category-pizza")
    public String pizzaPage() {
        return "category-pizza.html";
    }

    @RequestMapping("/category-drink")
    public String drinkPage() {
        return "category-drink.html";
    }

    @RequestMapping("/category-desert")
    public String desertPage() {
        return "category-desert.html";
    }

    @RequestMapping("/category-burger")
    public String burgerPage() {
        return "category-burger.html";
    }

}
