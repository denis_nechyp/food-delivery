package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.ProductsForOrderRequest;
import nechyporuk.fooddelivery.dto.response.ProductsForOrderResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.ProductsForOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("productsForOrder")
public class ProductsForOrderController {

    @Autowired
    private ProductsForOrderService service;

    @PostMapping
    public ProductsForOrderResponse save(@RequestBody ProductsForOrderRequest request) throws WrongInputException {
        return service.save(request);
    }

    @GetMapping
    public List<ProductsForOrderResponse> findAll() {
        return service.findAll();
    }

    @PutMapping
    public ProductsForOrderResponse update(@RequestParam Long id, @RequestBody ProductsForOrderRequest request) throws WrongInputException {
        return service.update(request, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) throws WrongInputException {
        service.delete(id);
    }
}
