package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.OrderRequest;
import nechyporuk.fooddelivery.dto.request.PaginationRequest;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.dto.response.OrderResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private OrderService orderService;

    @PostMapping
    public OrderResponse save(@RequestBody OrderRequest request) throws WrongInputException {
        return orderService.save(request);
    }

    @PostMapping("/page")
    public DataResponse<OrderResponse> findAll(@RequestBody PaginationRequest request) {
        return orderService.findAll(request);
    }

    @GetMapping
    public List<OrderResponse> findAll() {
        return orderService.findAll();
    }

    @PutMapping
    public OrderResponse update(@RequestParam Long id, @RequestBody  OrderRequest request) throws WrongInputException {
       return orderService.update(id, request);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) throws WrongInputException {
        orderService.delete(id);
    }



}
