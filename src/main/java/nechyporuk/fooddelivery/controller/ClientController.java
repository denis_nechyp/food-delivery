package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.ClientFilterRequest;
import nechyporuk.fooddelivery.dto.request.ClientRequest;
import nechyporuk.fooddelivery.dto.response.ClientResponse;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @PostMapping("/create")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ClientResponse save(@RequestBody @Valid ClientRequest clientRequest) throws WrongInputException {
        return clientService.save(clientRequest);
    }

    @PostMapping("/filter")
    public DataResponse<ClientResponse> findAllByFilter(@RequestBody ClientFilterRequest request){
        return clientService.findAllByFilter(request);
    }

    @GetMapping("/getOneById")
    public ClientResponse findOneById(@RequestParam Long id) throws WrongInputException {
        return clientService.findOneById(id);
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public List<ClientResponse> findAll() {
        return clientService.findAll();
    }

    @PutMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ClientResponse update(@RequestParam Long id, @RequestBody @Valid ClientRequest request) throws WrongInputException {
        return clientService.update(id, request);
    }

    @DeleteMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void delete(@RequestParam Long id) throws WrongInputException {
        clientService.delete(id);
    }

}
