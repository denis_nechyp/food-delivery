package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.BasketRequest;
import nechyporuk.fooddelivery.dto.response.BasketResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.BasketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/basket")
public class BasketController {

    @Autowired
    private BasketService basketService;

    @PostMapping("/create")
    public BasketResponse save(@RequestBody BasketRequest request) {
        return basketService.save(request);
    }

    @GetMapping
    public List<BasketResponse> findAll(){
        return basketService.findAll();
    }

    @PutMapping BasketResponse update (@RequestParam Long id, @RequestBody BasketRequest request) throws WrongInputException {
        return basketService.update(id, request);
    }

    @DeleteMapping
    public void delete(Long id) throws WrongInputException {
        basketService.delete(id);
    }
}
