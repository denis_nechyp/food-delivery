package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.CommentsRequest;
import nechyporuk.fooddelivery.dto.response.CommentsResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.CommentsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("comments")
public class CommentsController {

    @Autowired
    private CommentsService commentsService;

    @PostMapping
    public CommentsResponse save(@RequestBody CommentsRequest request) throws WrongInputException {
        return commentsService.save(request);
    }

    @GetMapping
    public List<CommentsResponse> findAll() {
        return commentsService.findAll();
    }

    @PutMapping
    public CommentsResponse update(@RequestParam Long id, @RequestBody CommentsRequest request) throws WrongInputException {
        return commentsService.update(request, id);
    }

    @DeleteMapping
    public void delete(@RequestParam Long id) throws WrongInputException {
        commentsService.delete(id);
    }
}
