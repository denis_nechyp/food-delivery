package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.CategoryRequest;
import nechyporuk.fooddelivery.dto.request.PaginationRequest;
import nechyporuk.fooddelivery.dto.response.CategoryResponse;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public CategoryResponse save(@RequestBody CategoryRequest categoryRequest) {
        return categoryService.save(categoryRequest);
    }

    @PostMapping("/page")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public DataResponse<CategoryResponse> findAll(@RequestBody PaginationRequest request) {
        return categoryService.findAll(request);
    }

    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public List<CategoryResponse> findAll() {
        return categoryService.findAll();
    }

    @GetMapping("/getOneById")
    public CategoryResponse findOneById(@RequestParam Long id) throws WrongInputException {
        return categoryService.findOneById(id);
    }

    @PutMapping
    public CategoryResponse update(@RequestBody CategoryRequest request, @RequestParam Long id) throws WrongInputException {
        return categoryService.update(request, id);
    }

    @DeleteMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void delete(@RequestParam Long id) throws WrongInputException {
        categoryService.delete(id);
    }
}
