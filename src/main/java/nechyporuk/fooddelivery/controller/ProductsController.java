package nechyporuk.fooddelivery.controller;

import nechyporuk.fooddelivery.dto.request.PaginationRequest;
import nechyporuk.fooddelivery.dto.request.ProductsRequest;
import nechyporuk.fooddelivery.dto.response.DataResponse;
import nechyporuk.fooddelivery.dto.response.ProductsResponse;
import nechyporuk.fooddelivery.exception.WrongInputException;
import nechyporuk.fooddelivery.service.ProductsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/products")
public class ProductsController {

    @Autowired
    private ProductsService productsService;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ProductsResponse save(@RequestBody ProductsRequest request) throws WrongInputException {
        return productsService.save(request);
    }

    @PostMapping("/page/byCategoryId")
    public DataResponse<ProductsResponse> findAllByCategoryId(@RequestParam Long categoryId, @RequestBody PaginationRequest request) throws WrongInputException {
        return productsService.findAllByCategoryId(categoryId, request);
    }

    @PostMapping("/page")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public DataResponse<ProductsResponse> findAll(@RequestBody PaginationRequest request) {
        return productsService.findAll(request);
    }

    @GetMapping
    public List<ProductsResponse> findAll() {
        return productsService.findAll();
    }

    @GetMapping("/getOneById")
    public ProductsResponse findOneById(@RequestParam Long id) throws WrongInputException {
        return productsService.findOneById(id);
    }

    @PutMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public ProductsResponse update(@RequestParam Long id, @RequestBody ProductsRequest request) throws WrongInputException {
        return productsService.update(request, id);
    }

    @DeleteMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    public void delete(@RequestParam Long id) throws WrongInputException {
        productsService.delete(id);
    }
}
