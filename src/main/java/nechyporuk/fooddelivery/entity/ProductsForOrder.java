package nechyporuk.fooddelivery.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class ProductsForOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Integer count;


    @ManyToMany(mappedBy = "productsForOrders")
    private List<Order> orders = new ArrayList<>();

    @ManyToOne
    private Products products;

    @ManyToOne
    private Basket basket;
}
