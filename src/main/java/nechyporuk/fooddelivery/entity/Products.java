package nechyporuk.fooddelivery.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
public class Products {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String picture;

    private Integer price;

    private Integer size;

    private Integer weight;

    private String ingredients;

    private Integer volume;

    @ManyToOne
    private Category category;


    @OneToMany(mappedBy = "products")
    private List<ProductsForOrder> productsForOrders = new ArrayList<>();


    @OneToMany(mappedBy = "products")
    private List<Comments> comments = new ArrayList<>();
}
