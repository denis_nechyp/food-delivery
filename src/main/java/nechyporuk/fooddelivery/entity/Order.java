package nechyporuk.fooddelivery.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor

@Entity
@Table(name = "_order")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String note;

    private LocalDateTime dateTime;

    private Integer payment;

    @ManyToOne
    private Client client;

    @ManyToMany
    private List<ProductsForOrder> productsForOrders = new ArrayList<>();
}
